const moment = require('moment');
const winston = require('winston');

const klogger = {
	logger: null,

	infoLogger: null,

	hide: false,

	updateConfig(config) {
		if (process.env.HIDE_LOGS) this.hide = true;
		this.infoLogger = new winston.Logger({
			transports: [new winston.transports.Console({
				json: false
			})]
		});

		this.logger = new winston.Logger({
			transports: [
				new winston.transports.Console({
					handleExceptions: config.handleExceptions || true,
					json: config.json || true
				})
			],
			exitOnError: config.exitOnError || false
		});

		// Load file transports if logIntoFile param is true
		if (config.logIntoFile) {
			this.date = moment().format('YYYY-MM-DD_HH-mm');

			this.path = config.file_path || __dirname.replace('node_modules/khem-log', '') + '/log/';

			this.files = {
				all: this.path + this.date + '_all.log',
				exceptions: this.path + this.date + '_exceptions.log'
			};

			this.logger.add(winston.transports.File, {
				filename: this.files.all
			});
			this.logger.add(winston.transports.File, {
				filename: this.files.exceptions
			});
		}
	},

	getMetadata(file, method, request) {
		var d = new Date();
		let meta = {
			date: d.toISOString()
		};
		if (request && request.headers) {
			meta.browser = request.headers['user-agent'] || 'undefined';
			const withoutproxy = request.connection.remoteAddress || 'undefined';
			meta.ip = request.headers['x-forwarded-for'] || withoutproxy;
		}
		if (file) meta.file = file;
		if (method) meta.method = method;
		return meta;
	},

	parseMessageType(error) {
		if (error instanceof Error) return error.message;
		if (typeof error == 'object') return error.message || JSON.stringify(error);
		return String(error);
	},

	log(error, file, functionName, request, type) {
		var metadata = this.getMetadata(file, functionName, request);
		message = this.parseMessageType(error);
		switch (type) {
			case 'info':
				this.infoLogger.info(message);
				break;
			case 'warning':
				this.logger.warn(message, metadata);
				break;
			case 'error':
				this.logger.error(message, metadata);
				break;
		}
	},

	info(msg, file, functionName, request) {
		if (!this.hide) this.log(msg, file, functionName, request, 'info');
	},

	warning(msg, file, functionName, request) {
		if (!this.hide) this.log(msg, file, functionName, request, 'warning');
	},

	error(msg, file, functionName, request) {
		if (!this.hide) this.log(msg, file, functionName, request, 'error');
	}
};

klogger.updateConfig({});

module.exports = klogger;