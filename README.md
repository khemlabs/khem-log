# README #

Khemlabs - Kickstarter - Khem log

### What is this repository for? ###

This lib is intended for developers that are using the khemlabs kickstarter framework, it
is a rapper of the wingston lib

### Requirements ###

> Khemlabs kickstarter server

> moment

> winston
  
### METHODS ###

> updateConfig(config: object): Update the wingston config

> info(error: [object, string], file: string, method: string, [request: object]): log level info

> warning(error: [object, string], file: string, method: string, [request: object]): log level warn

> error(error: [object, string], file: string, method: string, [request: object]): log level error

### Who do I talk to? ###

* dnangelus repo owner and admin
* developer elgambet and khemlabs